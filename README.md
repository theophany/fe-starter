# Stack

## Main

- Nextjs
- Typescript

## UI

- Chakra UI

## Query

- Axios
- React Query

## Form

- React Hook Form
- Yup

## State Management

- Zustand

## ORM

- Prisma

## SEO

- Next SEO
