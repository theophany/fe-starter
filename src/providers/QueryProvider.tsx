import { PropsWithChildren } from 'react';
import { QueryCache, QueryClient, QueryClientProvider } from 'react-query';

const queryCache = new QueryCache();
const client = new QueryClient({
  queryCache,
  defaultOptions: {
    queries: {
      staleTime: 0,
      refetchOnWindowFocus: false,
      cacheTime: 3 * 60 * 1000,
    },
  },
});

export const QueryProviders = ({ children }: PropsWithChildren) => {
  return <QueryClientProvider client={client}>{children}</QueryClientProvider>;
};
