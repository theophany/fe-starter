import { ChakraProvider, ColorModeProvider } from '@chakra-ui/react';
import { PropsWithChildren } from 'react';

import theme from '../config/theme';

export const ThemeProvider = ({ children }: PropsWithChildren) => {
  return (
    <ChakraProvider resetCSS theme={theme}>
      <ColorModeProvider
        options={{
          initialColorMode: 'light',
          useSystemColorMode: false,
        }}
      >
        {children}
      </ColorModeProvider>
    </ChakraProvider>
  );
};
