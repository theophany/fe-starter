import { ChakraProvider } from '@chakra-ui/react';
import { AppProps } from 'next/app';
import Head from 'next/head';

import theme from '../config/theme';
import { QueryProviders } from '../providers';
import Layout from '../uikit/components/Layout';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider resetCSS theme={theme}>
      <QueryProviders>
        <Head>
          <title>theosakram's starter</title>
        </Head>

        <main className="app">
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </main>
      </QueryProviders>
    </ChakraProvider>
  );
}

export default MyApp;
