import { Button, VStack } from '@chakra-ui/react';
import { FallbackProps } from 'react-error-boundary';

export const ErrorFallback = ({ error, resetErrorBoundary }: FallbackProps) => {
  return (
    <VStack>
      <h1>error: {error.message}</h1>
      <Button onClick={resetErrorBoundary}>Reset error</Button>
    </VStack>
  );
};
