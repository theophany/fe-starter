import { PropsWithChildren } from 'react';
import { ErrorBoundary } from 'react-error-boundary';

import { ErrorFallback } from './ErrorFallback';

interface ErrorGuardProps {
  onReset?: () => void;
}

export const ErrorGuard = ({
  children,
  onReset,
}: PropsWithChildren<ErrorGuardProps>) => {
  return (
    <ErrorBoundary onReset={onReset} FallbackComponent={ErrorFallback}>
      {children}
    </ErrorBoundary>
  );
};
