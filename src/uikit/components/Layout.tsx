import { Box, Center } from '@chakra-ui/react';
import { PropsWithChildren } from 'react';

const Layout = ({ children }: PropsWithChildren) => {
  return (
    <Center>
      <Box w="100%" maxW="96em">
        {children}
      </Box>
    </Center>
  );
};

export default Layout;
