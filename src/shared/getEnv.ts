import nextGetConfig from 'next/config';

interface Config {
  serverRuntimeConfig: {
    baseEndpoint: string;
  };
}

export const getEnv = () => nextGetConfig() as Config;
