import axios from 'axios';

import { getEnv } from './getEnv';

const {
  serverRuntimeConfig: { baseEndpoint },
} = getEnv();

export const fetcher = axios.create({
  baseURL: baseEndpoint,
  timeoutErrorMessage: 'Jaringan bermasalah',
});
