const baseEndpoint = process.env.BASE_ENDPOINT || '';

let nextConfig = {
  serverRuntimeConfig: {
    baseEndpoint,
  },
  webpack: (config) => {
    config.module.rules = [
      ...config.module.rules,
      {
        test: /src\/uikit\/*/i,
        sideEffects: false,
      },
    ];

    return config;
  },
};

module.exports = nextConfig;
